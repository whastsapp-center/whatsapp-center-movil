package com.miempresa.whatsapp_center_movil.whatsApp

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Pair
import android.view.View
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.miempresa.whatsapp_center_movil.R
import com.miempresa.whatsapp_center_movil.db.NumeroRepositorio
import com.miempresa.whatsapp_center_movil.db.UsuarioRepositorio
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        val usuarioRepositorio = UsuarioRepositorio()
        usuarioRepositorio.eliminar()

        val numeroRepositorio = NumeroRepositorio()
        numeroRepositorio.eliminar()

        val animacion1 = AnimationUtils.loadAnimation(this, R.anim.desplazamiento_arriba)
        val animacion2 = AnimationUtils.loadAnimation(this, R.anim.desplazamiento_abajo)

        imgLogoInicio.animation = animacion1
        lblNombreApp.animation = animacion2

        val r = Runnable {
            val intent = Intent(this, Login::class.java)
            val imgAnim = Pair.create<View?, String?>(imgLogoInicio, "imgLogoTrans")
            val textAnim = Pair.create<View?, String?>(lblNombreApp, "imgLogoTrans")
            val options = ActivityOptions.makeSceneTransitionAnimation(this, imgAnim, textAnim)
            startActivity(intent, options.toBundle())
        }
        Handler().postDelayed(r, 4000)

    }
}