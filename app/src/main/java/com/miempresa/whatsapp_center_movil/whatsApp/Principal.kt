package com.miempresa.whatsapp_center_movil.whatsApp


import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.navigation.NavigationView
import com.miempresa.whatsapp_center_movil.R
import com.miempresa.whatsapp_center_movil.db.NumeroRepositorio
import com.miempresa.whatsapp_center_movil.db.UsuarioRepositorio
import com.miempresa.whatsapp_center_movil.fragmentosApp.ContactosFragment
import com.miempresa.whatsapp_center_movil.services.Service
import kotlinx.android.synthetic.main.activity_principal.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONException


class Principal : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var contactosFragment: ContactosFragment
    private val servidor = Service()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)
        setSupportActionBar(toolbar)
        numero()
        ActionBarDrawerToggle(this, layoutPrincipal, R.string.open, R.string.close)
        val drawerLayout: DrawerLayout = layoutPrincipal
        val navigationView: NavigationView = nav_view
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.open, R.string.close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)
        val header = navigationView.getHeaderView(0)
        val nombre: TextView = header.findViewById(R.id.lblUsuario)
        val usuarioGuardado = UsuarioRepositorio().listar()
        nombre.text = usuarioGuardado[0].nombres

        val animacion = AnimationUtils.loadAnimation(this, R.anim.desplazamiento_arriba)
        imgLogoPrincipal.animation = animacion
    }

    private fun numero() {
        val usuario = UsuarioRepositorio().listar()
        val urlNumero = servidor.url + "numero/lista/${usuario[0].idempresa}"
        val queue = Volley.newRequestQueue(this)
        val stringRequest = JsonArrayRequest(urlNumero, { response ->
            try {
                for (i in 0 until response.length()) {
                    val idnumero = response.getJSONObject(i).getInt("idnumero")
                    val numero = response.getJSONObject(i).getString("numero")
                    val idempresa = response.getJSONObject(i).getInt("idempresa")
                    val razonsocial = response.getJSONObject(i).getString("razonsocial")
                    val numeroRepositorio = NumeroRepositorio()
                    numeroRepositorio.crear(idnumero, numero, idempresa, razonsocial)

                }
            } catch (e: JSONException) {
            }
        }, {
        })
        queue.add(stringRequest)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navContactos -> {
                contactosFragment = ContactosFragment()
                supportFragmentManager.beginTransaction().replace(
                    R.id.frameLayout,
                    contactosFragment
                ).setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_OPEN
                ).commit()
                supportActionBar?.title = ""
            }

            R.id.navCerrarSesion -> {
                val actividadLogin = Intent(this, MainActivity::class.java)
                startActivity(actividadLogin)
                finish()
            }

            R.id.navSalir -> {
                finishAffinity()
            }
        }
        layoutPrincipal.closeDrawer(GravityCompat.START)
        return true
    }


    override fun onBackPressed() {
        if (layoutPrincipal.isDrawerOpen(GravityCompat.START)) {
            layoutPrincipal.closeDrawer(GravityCompat.START)
        }
        else {
            super.onBackPressed()
        }
    }
}
