package com.miempresa.whatsapp_center_movil.whatsApp


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.view.View
import android.widget.Toast
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.miempresa.whatsapp_center_movil.R
import com.miempresa.whatsapp_center_movil.db.UsuarioRepositorio
import com.miempresa.whatsapp_center_movil.services.Service
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.imgLogo
import org.json.JSONException
import org.json.JSONObject

class Login : AppCompatActivity() {
    private val servidor = Service()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        btnLogin.setOnClickListener {
            login()
        }
    }

    private fun login() {
        val nombreUsuario = txtUsuario.text.toString()
        val clave = txtClave.text.toString()
        val queue = Volley.newRequestQueue(this)
        val url = servidor.url + "usuario/login"
        if (nombreUsuario.isNotEmpty() || clave.isNotEmpty()) {
            imgLogo.visibility = View.VISIBLE
            imgLogoError.visibility = View.GONE
            txtError.visibility = View.GONE
        }

        val jsonObject = JSONObject()
        jsonObject.put("usuario", nombreUsuario)
        jsonObject.put("clave", clave)

        val jsonObjectRequest = JsonObjectRequest(url, jsonObject, { response ->
                try {
                    val idusuario: Int = response.getInt("idusuario")
                    val idempresa: Int = response.getInt("idempresa")
                    val apellidos: String = response.getString("apellidos")
                    val nombres: String = response.getString("nombres")
                    val usuario: String = response.getString("usuario")
                    val idrol: Int = response.getInt("idrol")
                    val rol: String = response.getString("rol")

                    val usuarioRepositorio = UsuarioRepositorio()
                    usuarioRepositorio.crear(idusuario, idempresa, apellidos, nombres, usuario, idrol, rol)
                    val actividadPrinipal = Intent(this, Principal::class.java)
                    startActivity(actividadPrinipal)
                    finish()
                    Toast.makeText(applicationContext, "Bienvenido $nombres ", Toast.LENGTH_LONG).show()
                } catch (e: JSONException) {
                    imgLogo.visibility = View.GONE; imgLogoError.visibility = View.VISIBLE
                    txtError.visibility = View.VISIBLE
                    txtUsuario.setText(""); txtClave.setText("")
                }
            },
            {
                imgLogo.visibility = View.GONE; imgLogoError.visibility = View.VISIBLE
                txtError.visibility = View.VISIBLE
                txtUsuario.setText(""); txtClave.setText("")
            })
        queue.add(jsonObjectRequest)
    }

}
