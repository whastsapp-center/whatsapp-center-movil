package com.miempresa.whatsapp_center_movil.whatsApp

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.miempresa.whatsapp_center_movil.R
import com.miempresa.whatsapp_center_movil.adapter.AdaptadorMensajes
import com.miempresa.whatsapp_center_movil.modelos.Mensajes
import com.miempresa.whatsapp_center_movil.services.MensajeService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_chat.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class Chat : AppCompatActivity() {

    val listaMensajes = ArrayList<Mensajes>()
    val adapter = AdaptadorMensajes(listaMensajes)
    private val mensajeService = MensajeService()
    var idcontacto = 0

    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        setSupportActionBar(chatToolbar)
        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        chatMensajes.layoutManager = LinearLayoutManager(this)

        btnEnviarMensaje.isEnabled = false
        txtEnviarMensaje.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                btnEnviarMensaje.isEnabled = s.toString() != ""
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                btnEnviarMensaje.isEnabled = s.toString() != ""
            }
        })

        actualizarPrueba.setOnClickListener {
            finish()
            startActivity(intent)
        }

        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            idcontacto = bundle.getInt("idcontacto")
            val nombreContacto = bundle.getString("nombreContacto")
            val imagenContacto = bundle.getString("imagenContacto")

            nombreContactoChat.text = nombreContacto
            Picasso.get()
                .load(imagenContacto)
                .into(imagenContactoChat)

            val queue = Volley.newRequestQueue(this)
            val urlListar = mensajeService.listar(idcontacto)

            val stringRequest = JsonArrayRequest(urlListar, { response ->
                try {
                    for (i in 0 until response.length()) {
                        val idmensaje = response.getJSONObject(i).getString("id")
                        val body = response.getJSONObject(i).getString("body")
                        val fromMe = response.getJSONObject(i).getBoolean("fromMe")
                        val time = response.getJSONObject(i).getLong("time")
                        listaMensajes.add(Mensajes(idcontacto, idmensaje, body, fromMe, time))
                    }
                    chatMensajes.adapter = adapter

                    adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                            super.onItemRangeInserted(positionStart, itemCount)
                            chatMensajes.scrollToPosition(adapter.itemCount - 1)
                        }
                    })
                } catch (e: JSONException) {
                    println("Error -> $e")
                }
            }, {
            })
            queue.add(stringRequest)
        }

        btnEnviarMensaje.setOnClickListener {
            enviarMensaje()
            agregarMensaje()
            txtEnviarMensaje.setText("")
        }
    }

    private fun agregarMensaje() {
        val date = Date()
        val time = date.time / 1000
        val mensajes = Mensajes(idcontacto, listaMensajes[0].idmensaje, txtEnviarMensaje.text.toString(), true, time)
        listaMensajes.add(mensajes)
        adapter.notifyDataSetChanged()
    }

    private fun enviarMensaje() {
        val queue = Volley.newRequestQueue(this)
        val urlEnviarMensajes = mensajeService.urlEnviarMensaje
        val jsonObject = JSONObject()
        jsonObject.put("idcontacto", idcontacto)
        jsonObject.put("mensaje", txtEnviarMensaje.text.toString())

        val jsonObjectRequest = JsonObjectRequest(urlEnviarMensajes, jsonObject, { response ->
            try {
            } catch (e: JSONException) {
            }
        }, {})
        queue.add(jsonObjectRequest)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
