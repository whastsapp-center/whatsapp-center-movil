package com.miempresa.whatsapp_center_movil.fragmentosApp


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.miempresa.whatsapp_center_movil.R
import com.miempresa.whatsapp_center_movil.adapter.AdaptaderContacto
import com.miempresa.whatsapp_center_movil.db.NumeroRepositorio
import com.miempresa.whatsapp_center_movil.db.UsuarioRepositorio
import com.miempresa.whatsapp_center_movil.modelos.Contactos
import com.miempresa.whatsapp_center_movil.services.ContactoService
import com.miempresa.whatsapp_center_movil.services.Service
import kotlinx.android.synthetic.main.fragment_contactos.*
import org.json.JSONException


class ContactosFragment : Fragment() {

    private val listaContactos = ArrayList<Contactos>()
    private val adapter = AdaptaderContacto(listaContactos)
    private var servidor = Service()
    private val servidorContacto = ContactoService()
    private val numeroRepositorio = NumeroRepositorio()
    private val usuario = UsuarioRepositorio().listar()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_contactos, container, false)
    }

    @ExperimentalStdlibApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listaMensajes.layoutManager = LinearLayoutManager(context)
        mostrarNumero()
        swipRefresh.setOnRefreshListener {
            listarContactos()
            listaContactos.clear()
        }
        listarContactos()
    }

    private fun mostrarNumero() {
        val listaOpciones = view?.findViewById<Spinner>(R.id.spiNumeros)
        val urlNumero = servidor.url + "numero/lista/${usuario[0].idempresa}"
        val queue = Volley.newRequestQueue(context)
        val stringRequest = JsonArrayRequest(urlNumero, { response ->
            try {
                val contactos: ArrayList<String> = ArrayList()
                for (i in 0 until response.length()) {
                    val numero = response.getJSONObject(i).getString("numero")
                    contactos.add(numero)
                }
                val adapter = context?.let {
                    ArrayAdapter(it, android.R.layout.simple_list_item_1, android.R.id.text1, contactos)
                }
                if (listaOpciones != null) { listaOpciones.adapter = adapter }
            } catch (e: JSONException) {
                Toast.makeText(context, "Error al obtener los datos", Toast.LENGTH_SHORT).show()
            }
        }, {
            Toast.makeText(context, "Verifique si está conectado a internet", Toast.LENGTH_SHORT).show()
        })
        queue.add(stringRequest)
    }

    private fun listarContactos() {
        swipRefresh.isRefreshing = true
        val idnumero = numeroRepositorio.listar()[0].idnumero.toString().toInt()
        val idusuario = usuario[0].idusuario.toString().toInt()
        val urlContactos = servidorContacto.listar(idnumero, idusuario)
        val queue = Volley.newRequestQueue(context)
        val stringRequest = JsonArrayRequest(urlContactos, { response ->
            try {
                swipRefresh.isRefreshing = false
                for (i in 0 until response.length()) {
                    val idcontacto = response.getJSONObject(i).getInt("idcontacto")
                    val imagen = response.getJSONObject(i).getString("imagen")
                    val nombre = response.getJSONObject(i).getString("nombre")
                    val fconexion = response.getJSONObject(i).getString("fconexion")
                    val nombreUsuario = response.getJSONObject(i).getString("nombreusuario")
                    listaContactos.add(
                            Contactos(idcontacto, imagen, nombre, fconexion, nombreUsuario)
                    )
                    adapter.notifyDataSetChanged()
                }
                listaMensajes.adapter = adapter

            } catch (e: JSONException) {
                swipRefresh.isRefreshing = false
                println("Error -> $e")
            }
        }, {
        })
        queue.add(stringRequest)
    }
}
