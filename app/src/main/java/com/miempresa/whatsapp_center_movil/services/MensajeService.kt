package com.miempresa.whatsapp_center_movil.services


class MensajeService {
    val service = Service()

    val urlListar = service.url + "mensaje/lista/"
    val urlEnviarMensaje = service.url + "mensaje/enviar/"
    val urlEliminar = service.url + "mensaje/eliminar/"

    fun listar(idcontacto: Int): String {
        return urlListar + idcontacto
    }

    fun eliminarMensaje(idcontacto: Int, idmensaje: String): String {
        return "$urlEliminar$idcontacto/$idmensaje"
    }
}
