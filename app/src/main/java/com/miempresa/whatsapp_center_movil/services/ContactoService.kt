package com.miempresa.whatsapp_center_movil.services

class ContactoService {
    val service = Service()

    val urlListar = service.url + "contacto/lista"



    fun listar(idnumero: Int, idusuario: Int): String {
        return "$urlListar/$idnumero/$idusuario"
    }
}
