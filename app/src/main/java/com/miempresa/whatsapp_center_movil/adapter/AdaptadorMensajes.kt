package com.miempresa.whatsapp_center_movil.adapter

import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.miempresa.whatsapp_center_movil.R
import com.miempresa.whatsapp_center_movil.db.UsuarioRepositorio
import com.miempresa.whatsapp_center_movil.modelos.Mensajes
import com.miempresa.whatsapp_center_movil.services.MensajeService
import kotlinx.android.synthetic.main.mensajes_chat.view.*
import org.ocpsoft.prettytime.PrettyTime
import java.util.*
import kotlin.collections.ArrayList


class AdaptadorMensajes(val listaMensajes: ArrayList<Mensajes>): RecyclerView.Adapter<AdaptadorMensajes.ViewHolder>() {

    override fun getItemCount(): Int = listaMensajes.size;

    class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        fun render(mensajes: Mensajes) {
            val usuario = UsuarioRepositorio().listar()
            if (usuario[0].rol == "ADMINISTRADOR") {
                view.btnEliminarMensaje.visibility = View.VISIBLE
            }
            else {
                view.btnEliminarMensaje.visibility = View.GONE
            }
            val date = Date(mensajes.time * 1000L)
            val prettyTime = PrettyTime(Date(), Locale.getDefault())
            val tiempo = prettyTime.format(date)

            if (mensajes.fromMe) {
                view.layoutMensajeRecibido.visibility = View.GONE
                view.mensajeEnviado.text = mensajes.mensaje
                view.horaMensajeEnviado.text = tiempo
            }

            else if (!mensajes.fromMe) {
                view.layoutMensajeEnviado.visibility = View.GONE
                view.mensajeRecibido.text = mensajes.mensaje
                view.horaMensajeRecibido.text = tiempo
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.mensajes_chat, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.render(listaMensajes[position])
        val infoData: Mensajes = listaMensajes[position]
        holder.view.btnEliminarMensaje.setOnClickListener {
            val dialog = Dialog(holder.view.context)
            dialog.setContentView(R.layout.dialogo_personalizado)
            dialog.window?.setBackgroundDrawableResource(R.drawable.fondo_dialogo)
            dialog.setCancelable(false)
            dialog.window?.attributes?.windowAnimations = R.style.animation
            val aceptar = dialog.findViewById<Button>(R.id.dialogAceptar)
            val cancelar = dialog.findViewById<Button>(R.id.dialogCancelar)
            aceptar.setOnClickListener {
                val queue = Volley.newRequestQueue(holder.view.context)
                val urlEliminarMensaje = MensajeService().eliminarMensaje(listaMensajes[position].idcontacto, listaMensajes[position].idmensaje)
                val postRequest: StringRequest = object : StringRequest(Method.DELETE,
                        urlEliminarMensaje, {},{}
                ){}
                queue.add(postRequest)
                dialog.dismiss()
                eliminarMensaje(infoData)
            }
            cancelar.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    private fun eliminarMensaje(infoData: Mensajes) {
        val position = listaMensajes.indexOf(infoData)
        listaMensajes.removeAt(position)
        notifyItemRemoved(position)
    }
}
