package com.miempresa.whatsapp_center_movil.adapter


import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.miempresa.whatsapp_center_movil.R
import com.miempresa.whatsapp_center_movil.modelos.Contactos
import com.miempresa.whatsapp_center_movil.whatsApp.Chat
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.elementos_lista.view.*

class AdaptaderContacto (val listaContactos: ArrayList<Contactos>): RecyclerView.
Adapter<AdaptaderContacto.ViewHolder>() {

    override fun getItemCount(): Int = listaContactos.size

    class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        fun render(contactos: Contactos) {
            view.nombreContacto.text = contactos.nombre
            view.fechaConexion.text = contactos.fconexion
            view.asignado.text = contactos.nombreusuario

            Picasso.get()
                    .load(contactos.imagenContacto)
                    .into(view.imagenContacto)

            when (contactos.nombreusuario) {
                "JHON - POLAR" -> { view.asignado.setTextColor(Color.parseColor("#046CB4")) }
                "JORDY - CARITA" -> { view.asignado.setTextColor(Color.parseColor("#CE3B3B")) }
                "CARLOS - OHASHI" -> { view.asignado.setTextColor(Color.parseColor("#2BB51A")) }
            }

            view.setOnClickListener {
                val intent = Intent(view.context, Chat::class.java)
                intent.putExtra("idcontacto", contactos.idcontacto)
                intent.putExtra("nombreContacto", contactos.nombre)
                intent.putExtra("imagenContacto", contactos.imagenContacto)
                ContextCompat.startActivity(view.context, intent, Bundle())
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.elementos_lista, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.render(listaContactos[position])
    }

}