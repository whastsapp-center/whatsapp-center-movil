package com.miempresa.whatsapp_center_movil.modelos

data class Mensajes(val idcontacto: Int, val idmensaje: String,
                    val mensaje: String, val fromMe: Boolean, val time: Long)
