package com.miempresa.whatsapp_center_movil.modelos

data class Contactos (
    val idcontacto: Int, val imagenContacto:String, val nombre:String, val fconexion:String, val nombreusuario: String
)
