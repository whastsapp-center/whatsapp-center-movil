package com.miempresa.whatsapp_center_movil.db
import com.orm.SugarRecord


class NumeroRepositorio {
    fun crear(idnumero: Int, numero: String, idempresa: Int, razonsocial: String) {
        val nuevoNumero = Numero(
                idnumero, numero, idempresa, razonsocial
        )
        SugarRecord.save(nuevoNumero)
    }

    fun listar(): List<Numero> {
        val numero = SugarRecord.listAll(Numero::class.java)
        return numero
    }

    fun eliminar() {
        SugarRecord.deleteAll(Numero::class.java)
    }
}