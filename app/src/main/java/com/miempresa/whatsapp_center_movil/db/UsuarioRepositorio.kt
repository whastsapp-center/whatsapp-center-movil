package com.miempresa.whatsapp_center_movil.db
import com.orm.SugarRecord


class UsuarioRepositorio {
    fun crear(
        idusuario: Int, idempresa: Int, apellidos: String, nombres: String,
        usuario: String, idrol: Int, rol: String
    ) {
        val nuevoUsuario = Usuario(
            idusuario, idempresa, apellidos, nombres,
            usuario, idrol, rol
        )
        SugarRecord.save(nuevoUsuario)
    }

    fun listar(): List<Usuario> {
        val usuario = SugarRecord.listAll(Usuario::class.java)
        return usuario
    }


    fun eliminar() {
        SugarRecord.deleteAll(Usuario::class.java)
    }
}