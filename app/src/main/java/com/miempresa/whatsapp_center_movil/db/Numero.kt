package com.miempresa.whatsapp_center_movil.db
import com.orm.dsl.Table


@Table
data class Numero (
        var idnumero: Int? = null,
        var numero: String? = null,
        var razonsocial: String? = null,
        var idempresa: Int? = null) {

    constructor(idnumero: Int?, numero: String?, idempresa: Int?, razonsocial: String?): this() {
        this.idnumero = idnumero
        this.numero = numero
        this.idempresa = idempresa
        this.razonsocial = razonsocial
    }
}