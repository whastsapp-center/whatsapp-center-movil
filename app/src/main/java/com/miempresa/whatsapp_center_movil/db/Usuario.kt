package com.miempresa.whatsapp_center_movil.db
import com.orm.dsl.Table


@Table
data class Usuario (
        var idusuario: Int? = null,
        var idempresa: Int? = null,
        var apellidos: String? = null,
        var nombres: String? = null,
        var idrol: Int? = null,
        var rol: String? = null,
        var usuario: String? = null ) {

    constructor(idusuario: Int?, idempresa: Int?, apellidos: String?, nombres: String?,
                usuario: String?, idrol: Int?, rol: String?): this() {
        this.idusuario = idusuario
        this.idempresa = idempresa
        this.apellidos = apellidos
        this.nombres = nombres
        this.usuario = usuario
        this.idrol = idrol
        this.rol = rol
    }
}
